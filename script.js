function loaded() {
	let varColor = 'black';
	document.getElementById("color").oninput = function() {
		varColor = this.value;
	};
	let canvas = document.getElementById("canvas");
	let ctx = canvas.getContext("2d");
	document.getElementById("save").onclick = function() {
		let dataURL = canvas.toDataURL("image/jpeg");
		let link = document.createElement("a");
		link.href = dataURL;
		link.download = "image.jpg";
		link.click();
	}
	document.getElementById("clear").onclick = function() {
		ctx.clearRect(0, 0, 1000, 500);
	}
	ctx.fillStyle = varColor;
	let xPrev = null;
	let yPrev = null;
	let size = 10;
	document.getElementById("chooseSize").oninput = function() {
		size = this.value;
	}
	canvas.onmousedown = function(event) {
		let x = event.offsetX;
		let y = event.offsetY;
		ctx.fillStyle = varColor;
		ctx.fillRect(x-size/2, y-size/2, size, size);
		canvas.onmousemove = function(event) {
			let x = event.offsetX;
			let y = event.offsetY;
			if (xPrev != null && yPrev != null) {
				ctx.beginPath();
				ctx.lineCap = "round";
				ctx.lineWidth = size*Math.sqrt(2);
				ctx.strokeStyle = varColor;
				ctx.fillStyle = varColor;
				ctx.moveTo(xPrev, yPrev);
				ctx.lineTo(x, y);
				ctx.stroke();
			};
			ctx.fillStyle = varColor;
			ctx.fillRect(x-size/2, y-size/2, size, size);
			xPrev = x;
			yPrev = y;
					}
		canvas.onmouseup = function() {
			canvas.onmousemove = null;
			xPrev = null;
			yPrev = null;
		}
	}
}
